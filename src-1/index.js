const express = require('express');

const app = express();

const PORT = 3003;

app.use(express.json());

app.post('/pos', (request, response) => {
  const email = request.body.email;
  const name = request.body.name;
  const password = request.body.password;

  return response.json({ name: name, email: email, password: password });
});
app.post('/res', (request, response) => {
  const { email, password, name } = request.body;

  return response.json({ name, email, password });
});
let data = [
  {
    name: 'mateus',
    email: 'hjunor@mail.com',
    password: '9999',
    value: 3,
  },
  {
    name: 'Ana',
    email: 'hjunor@mail.com',
    password: '9999',
    value: 40,
  },
  {
    name: 'Otavio',
    email: 'hjunor@mail.com',
    password: '9999',
    value: 89,
  },
  {
    name: 'Pedro',
    email: 'hjunor@mail.com',
    password: '9999',
    value: 34,
  },
  {
    name: 'Thiago',
    email: 'hjunor@mail.com',
    password: '9999',
    value: 33,
  },
  {
    name: 'Brener',
    email: 'hjunor@mail.com',
    password: '9999',
    value: 28,
  },
  {
    name: 'Rafael',
    email: 'hjunor@mail.com',
    password: '9999',
    value: 24,
  },
];

app.get('/name', (request, response) => {
  const name = data.map((item) => item.name);
  return response.json(name);
});

app.get('/value', (request, response) => {
  const value = data.reduce(
    (cb, item) => {
      return parseInt(cb) + parseInt(item.value);
    },
    [0]
  );
  return response.json(value);
});

app.get('/email', (request, response) => {
  let email = [];

  for (i = 0; i < data.length; i++) {
    email.push(data[i].email);
  }
  return response.json(email);
});

app.listen(PORT, () => {
  console.log('server run');
});
