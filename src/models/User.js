require('dotenv').config();
const mongoose = require('mongoose');
const uuid = require('uuid');
const bcrypt = require('bcrypt');
const ROUND = process.env.ROUND;
const UserShema = new mongoose.Schema(
  {
    _id: {
      type: String,
      default: uuid.v4(),
    },
    name: {
      require: true,
      type: String,
    },
    email: {
      type: String,
      unique: true,
      require: true,
      lowercase: true,
      match: [/\S+@\S+\.\S+/],
    },
    password: {
      type: String,
      require: true,
      select: true,
    },
  },
  {
    timestamps: true,
  }
);

UserShema.pre('save', async function (next) {
  const hash = await bcrypt.hash(this.password, ROUND);
  this.password = hash;
  next();
});

module.exports = mongoose.model('User', UserShema);
