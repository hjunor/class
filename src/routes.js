const router = require('express').Router();
const UserController = require('./controllers/User');

router.post('/', UserController.create);

module.exports = router;
