const UserShema = require('../models/User');

class UserController {
  async create(request, response) {
    try {
      const { email, password, name } = request.body;

      const newUser = await UserShema.create({
        name,
        email,
        password,
      });

      if (!newUser) {
        return response.status(400).json({ message: 'error' });
      }

      return response.status(201).json({ newUser });
    } catch (error) {
      console.log(error);
      return response.status(500).json({ message: 'error servidor' });
    }
  }
}

module.exports = new UserController();
